//
//  ShineView.swift
//  Shine
//
//  Created by reinier van vliet on 29/11/2019.
//  Copyright © 2019 reinier van vliet. All rights reserved.
//

import UIKit

public class ShineView: UIView {

    public var innerThickness = 0.8
    public var edgeThickness = 0.8
    public var innerAlpha = 0.7
    public var edgeAlpha = 1.0
    public var edgeWidth = 6.0

    private var displayLink: CADisplayLink?
    private var startTime = 0.0
    private let animLength = 2.0
    private let edgeShineBackground: CAGradientLayer
    private let innerShineBackground: CAGradientLayer

    public override init(frame: CGRect) {
        edgeShineBackground = CAGradientLayer()
        innerShineBackground = CAGradientLayer()
        super.init(frame: frame)

        startShine()
    }

    public required init?(coder: NSCoder) {
        edgeShineBackground = CAGradientLayer()
        innerShineBackground = CAGradientLayer()
        super.init(coder: coder)
    }

    private func startShine() {
        let maxWidth = max(innerShineBackground.frame.width, edgeShineBackground.frame.width)

        // Tweak to make the center of the shine be precisely cut through the corners at the same time
        let edgeCenterTweak = frame.width * CGFloat(edgeThickness) / 2
        let innerCenterTweak = frame.width * CGFloat(innerThickness) / 2

        // edge animation
        let edgeAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.position))
        edgeAnimation.fromValue = CGPoint(x: -maxWidth - edgeCenterTweak, y: 0.0)
        edgeAnimation.toValue = CGPoint(x: maxWidth - edgeCenterTweak, y: 0.0)
        edgeAnimation.duration = animLength
        edgeShineBackground.add(edgeAnimation, forKey: "position")

        // inner animation
        let innerAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.position))
        innerAnimation.fromValue = CGPoint(x: -maxWidth*3 - innerCenterTweak, y: 0.0)
        innerAnimation.toValue = CGPoint(x: maxWidth*3 - innerCenterTweak, y: 0.0)
        innerAnimation.duration = animLength
        innerShineBackground.add(innerAnimation, forKey: "position")
    }
    
    private func prepareShine() {
        edgeShineBackground.colors = [UIColor(white: 1.0, alpha: 0.0).cgColor,
                                      UIColor(white: 1.0, alpha: CGFloat(edgeAlpha)).cgColor,
                                      UIColor(white: 1.0, alpha: 0.0).cgColor]

        innerShineBackground.colors = [UIColor(white: 1.0, alpha: 0.0).cgColor,
                                       UIColor(white: 1.0, alpha: CGFloat(innerAlpha)).cgColor,
                                       UIColor(white: 1.0, alpha: 0.0).cgColor ]

        calcGradientPos(gradient: edgeShineBackground, thickness: edgeThickness)
        calcGradientPos(gradient: innerShineBackground, thickness: innerThickness)

        let innerMask = maskLayer(gradient: innerShineBackground, strokeColor: UIColor.clear, fillColor: UIColor.white)
        let edgeMask = maskLayer(gradient: edgeShineBackground, strokeColor: UIColor.white, fillColor: UIColor.clear)
        layer.addSublayer(innerMask)
        layer.addSublayer(edgeMask)
    }

    private func maskLayer(gradient: CAGradientLayer, strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let maskLayer = CAShapeLayer()
        maskLayer.addSublayer(gradient)
        maskLayer.cornerRadius = layer.cornerRadius
        maskLayer.fillColor = UIColor.white.cgColor
        maskLayer.frame = bounds
        maskLayer.masksToBounds = true
        let maskLayerMask = CAShapeLayer()
        maskLayerMask.path = shineMaskPath()
        maskLayerMask.lineWidth = CGFloat(edgeWidth)
        maskLayerMask.strokeColor = strokeColor.cgColor
        maskLayerMask.fillColor = fillColor.cgColor
        maskLayer.mask = maskLayerMask
        return maskLayer
    }

    private func calcGradientPos(gradient: CAGradientLayer, thickness: Double) {
        let halfThickness = thickness / 2
        let balkX1 = 0.0 + halfThickness
        let balkX2 = 1.0 - halfThickness
        let angle = atan2(balkX2-balkX1, 1.0)
        let startX = 0.5+cos(angle) * halfThickness
        let startY = 0.5+sin(angle) * halfThickness
        let endX = 0.5-cos(angle) * halfThickness
        let endY = 0.5-sin(angle) * halfThickness

        gradient.startPoint = CGPoint(x: startX, y: startY)
        gradient.endPoint = CGPoint(x: endX, y: endY)
        gradient.anchorPoint = CGPoint(x: 0, y: 0)
    }

    private func shineMaskPath() -> CGPath {
        let maskPath: CGPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        return maskPath
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        let baseWidth = frame.size.width
        let innerGradientWidth = baseWidth * CGFloat(innerThickness)
        let innerBarWidth = baseWidth + innerGradientWidth
        let innerBackgroundRect = CGRect(x: -innerBarWidth, y: 0.0, width: innerBarWidth, height: frame.height)
        let edgeGradientWidth = baseWidth * CGFloat(edgeThickness)
        let edgeBarWidth = baseWidth + edgeGradientWidth
        let edgeBackgroundRect = CGRect(x: -edgeBarWidth, y: 0.0, width: edgeBarWidth, height: frame.height)

        CATransaction.begin()
        CATransaction.disableActions()
        prepareShine()
        edgeShineBackground.frame = edgeBackgroundRect
        innerShineBackground.frame = innerBackgroundRect
        CATransaction.commit()
        startShine()
    }
}
