//
//  ViewController.swift
//  Shine
//
//  Created by reinier van vliet on 29/11/2019.
//  Copyright © 2019 reinier van vliet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        let tile = ShineView(frame: CGRect(x: 130.0, y: 130.0, width: 150.0, height: 150.0))
        tile.backgroundColor = .blue
        tile.layer.cornerRadius = 32.0
        tile.layer.shadowColor = UIColor.black.cgColor
        tile.layer.shadowOffset = CGSize(width: 6.0, height: 6.0)
        tile.layer.shadowOpacity = 0.5
        view.addSubview(tile)
    }

    
    

}

