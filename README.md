# README #

Simple example how to add a shine to any UIView in your app

Instead of inheriting from UIView, inherit from ShineView and you're done
Tweak the properties for extra effects

[![ShineFullTile](./ShineEmptyTile.gif "Example of an empty tile and a shine over it")]


[![ShineFullTile](./ShineFullTile.gif "Example of a tile full with info and a shine over it")]

